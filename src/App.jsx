// importation d'Axios
import axios from "axios"
import { useState, useEffect } from "react";
import "./App.css";
import TodoList from "./components/TodoList";

const API_URL = "https://jsonplaceholder.typicode.com/todos";

function App() {
  const [todos, setTodos] = useState([]);

  // hook useEffect
  useEffect(() => {
    // Encoi d'une requête GET & utilisation d'un callback .then pour récupérer les données
    axios.get(API_URL).then((response) => {
      // Modification de la valeur de todos via setTodos
      setTodos(response.data)
    });
  }, []);

  return (
    <div className="App">
      <TodoList todos={todos} />
    </div>
  );
}

export default App;
